import IRoute from "../interfaces/route.interface";
import SignUpPage from "../pages/auth/SignUpPage";
import HomePage from "../pages/HomePage";
import EventDetails from "../pages/EventDetails";
import Dashboard from "../pages/Dashboard";

const routes: IRoute[] = [
  {
    path: "/",
    exact: true,
    component: HomePage,
    name: "Home Page",
    protected: false,
  },
  {
    path: "/dashboard",
    exact: true,
    component: Dashboard,
    name: "Dashboard Page",
    protected: true,
  },
  {
    path: "/details",
    exact: true,
    component: EventDetails,
    name: "Details Page",
    protected: true,
  },
  {
    path: "/auth/signup",
    exact: true,
    component: SignUpPage,
    name: "Login Page",
    protected: false,
  },
];
export default routes;
