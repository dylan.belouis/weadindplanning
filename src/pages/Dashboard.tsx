import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import IPageProps from "../interfaces/page.interface";
import { todosRef } from "../config/firebase";
import "../styles/Dashboard.css";

const Dashboard: React.FunctionComponent<IPageProps> = (props) => {
  const [events, setEvents] = useState<any[]>([]);
  const [name, setName] = useState<string>("");
  const [description, setDescription] = useState<string>("");
  const [guestNumber, setGuestNumber] = useState<string>("");
  const [date, setDate] = useState<string>("");
  const [toggleForm, setToggleForm] = useState<boolean>(false);

  useEffect(() => {
    todosRef.on("value", (snapshot) => {
      setEvents(snapshot.val());
    });
    console.log(events);
  }, []);

  const createEvent = (e: React.FormEvent<EventTarget>) => {
    e.preventDefault();
    const item = {
      name: name,
      description: description,
      guestNumber: guestNumber,
      date: date,
      done: false,
    };
    todosRef.push(item);
    setName("");
    setDescription("");
    setDate("");
  };

  return (
    <div>
      <h1>Weading planning</h1>
      <Link to={`/`}>
        <button>Page d'acceuil</button>
      </Link>
      <button onClick={() => console.log(events)}>console.log</button>
      <h2>Evenements</h2>
      <ul className="ul_event_list_Dashboard">
        {Object.keys(events) &&
          Object.keys(events).map((key: any) => {
            return (
              <li className="li_event_list_Dashboard" key={key}>
                <p className="event_item_Dashboard">{events[key].name}</p>
                {/* <p className="event_item_Dashboard">{events[key].description}</p> */}
                {/* <p className="event_item_Dashboard">{events[key].guestNumber}</p> */}
                <p className="event_item_Dashboard">{events[key].date}</p>
                <Link to={`/details?id=${key}`}>
                  <button>Gerer cet evenement</button>
                </Link>
              </li>
            );
          })}
      </ul>
      <p>Creer votre evenement</p>
      {toggleForm && (
        <form className="create_event_form_Dashboard" onSubmit={createEvent}>
          <p>Entrez un nom</p>
          <input
            id="outlined-basic"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
          <p>Entrez une description</p>
          <input
            id="outlined-basic"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
          />
          <p>Votre nombre d'invités</p>
          <input
            id="outlined-basic"
            value={guestNumber}
            onChange={(e) => setGuestNumber(e.target.value)}
          />
          <p>Entrez une date</p>
          <input
            id="outlined-basic"
            value={date}
            onChange={(e) => setDate(e.target.value)}
          />
          <input
            className="button_create_event_Dashboard"
            type="submit"
            onClick={createEvent}
            value="Valider"
          />
        </form>
      )}
      <div className="box_create_button_Dashboard">
        <button
          className="button_create_event_Dashboard"
          onClick={() => {
            setToggleForm(!toggleForm);
          }}
        >
          {toggleForm ? "Annuler" : "Creer"}
        </button>
      </div>
    </div>
  );
};
export default Dashboard;
