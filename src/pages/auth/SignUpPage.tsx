import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import firebase from "firebase/compat";

import IPageProps from "../../interfaces/page.interface";
import { SignInWithSocialMedia } from "../../modules/auth";
import { Providers } from "../../config/firebase";

const SignUpPage: React.FunctionComponent<IPageProps> = (props) => {
  const [authenticating, setAuthenticating] = useState<boolean>(false);
  const [error, setError] = useState<string>("");
  const history = useHistory();

  const signInWithSocialMedia = (provider: firebase.auth.AuthProvider) => {
    if (error !== "") setError("");

    setAuthenticating(true);

    SignInWithSocialMedia(provider)
      .then((result) => {
        let test = result.additionalUserInfo?.profile;
        // const lowerUrl = test.toLowerCase();
        // const regex = lowerUrl.replace(/ /g, "+");
        console.log(test);
        history.push("/");
      })
      .catch((error) => {
        setAuthenticating(false);
        setError(error.message);
      });
  };

  return (
    <div className="AuthLogin">
      <div className="auth-main-container">
        <div>
          <h1>Creer un compte</h1>
          <p>
            Enregistrez votre compte ou connectez vous pour acceder à votre
            plannification d'evenement.
          </p>
        </div>
        <div className="auth-btn-wrapper">
          <button
            disabled={authenticating}
            onClick={() => signInWithSocialMedia(Providers.google)}
          >
            S'inscrire avec Google
          </button>
          <button
            disabled={authenticating}
            onClick={() => signInWithSocialMedia(Providers.facebook)}
          >
            S'inscrire avec Facebook
          </button>
          <Link to={`/`}>
            <button>Retourner à la page d'acceuil</button>
          </Link>
          <Link to={`/dashboard`}>
            <button>Site</button>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default SignUpPage;
