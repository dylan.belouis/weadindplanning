import React, { useState, useEffect } from "react";
import IPageProps from "../interfaces/page.interface";
import { databaseRef } from "../config/firebase";

const EventDetails: React.FunctionComponent<IPageProps> = (props) => {
  const [event, setEvent] = useState<any>({});
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  const id = urlParams.get("id");
  const ref = databaseRef.child(`event/${id}`);
  const [toggleGiftForm, setToggleGiftForm] = useState<boolean>(false);
  const [toggleGameForm, setToggleGameForm] = useState<boolean>(false);
  const [gift, setGift] = useState<string>("");
  const [game, setGame] = useState<string>("");

  useEffect(() => {
    ref.on("value", (snapshot) => {
      setEvent(snapshot.val());
    });
    console.log(event);
    console.log(ref);
  }, []);

  const createGift = (e: React.FormEvent<EventTarget>) => {
    e.preventDefault();
    const item = {
      gift: gift,
    };
    ref.push({ gift: gift });
    setGift("");
    setToggleGiftForm(!toggleGiftForm);
  };

  const createGame = (e: React.FormEvent<EventTarget>) => {
    e.preventDefault();
    const item = {
      game: game,
    };
    ref.push(item);
    setGame("");
    setToggleGameForm(!toggleGameForm);
  };

  const removeGift = (e: React.FormEvent<EventTarget>, ref2: any) => {
    e.preventDefault();
    const removeRef = databaseRef.child(`event/${id}/${ref2}`);
    removeRef.remove();
  };

  return (
    <div className="EventDetail">
      <button onClick={() => console.log(event)}>test</button>
      <button onClick={() => window.history.back()}>retour</button>
      <h2>EventDetail</h2>
      <p>
        Evenement:{` `}
        {event.name}
      </p>
      <p>
        Description:{` `}
        {event.description}
      </p>
      <p>
        Nombre d'invités:{` `}
        {event.guestNumber}
      </p>
      <p>
        Date:{` `}
        {event.date}
      </p>
      <h4>Cadeau</h4>
      {Object.keys(event) ? (
        Object.keys(event).map((key: any) => {
          return (
            <div key={key}>
              <p>{event[key].gift}</p>
            </div>
          );
        })
      ) : (
        <p>Aucun cadeau</p>
      )}
      <button onClick={() => setToggleGiftForm(!toggleGiftForm)}>
        Ajouter un cadeau
      </button>
      {toggleGiftForm && (
        <form onSubmit={createGift}>
          <input
            type="text"
            placeholder="Cadeau"
            value={gift}
            onChange={(e) => setGift(e.target.value)}
          />
          <input type="submit" onSubmit={createGift} />
        </form>
      )}
      <h2>Proposition de jeux</h2>
      {Object.keys(event) ? (
        Object.keys(event).map((key: any) => {
          return (
            <div key={key}>
              <p>{event[key].game}</p>
              <button onClick={(e) => removeGift(e, event[key].game)}>
                Supprimer
              </button>
            </div>
          );
        })
      ) : (
        <p>Aucun jeux</p>
      )}
      <button onClick={() => setToggleGameForm(!toggleGameForm)}>
        Ajouter un jeux
      </button>
      {toggleGameForm && (
        <form onSubmit={createGame}>
          <input
            type="text"
            placeholder="Jeux"
            value={game}
            onChange={(e) => setGame(e.target.value)}
          />
          <input type="submit" onSubmit={createGame} />
        </form>
      )}
    </div>
  );
};

export default EventDetails;

// Idées de Jeux, Animations, etc.
