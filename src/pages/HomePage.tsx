import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import firebase from "firebase/compat";
import IPageProps from "../interfaces/page.interface";
import "../styles/HomePage.css";
import { SignInWithSocialMedia } from "../modules/auth";
import { Providers } from "../config/firebase";

const HomePage: React.FunctionComponent<IPageProps> = (props) => {
  const [authenticating, setAuthenticating] = useState<boolean>(false);
  const [error, setError] = useState<string>("");
  const history = useHistory();

  const signInWithSocialMedia = (provider: firebase.auth.AuthProvider) => {
    if (error !== "") setError("");

    setAuthenticating(true);

    SignInWithSocialMedia(provider)
      .then((result) => {
        let test = result.additionalUserInfo?.profile;
        // const lowerUrl = test.toLowerCase();
        // const regex = lowerUrl.replace(/ /g, "+");
        console.log(test);
        history.push("/");
      })
      .catch((error) => {
        setAuthenticating(false);
        setError(error.message);
      });
  };

  return (
    <div className="HomePage">
      <h1>Weading Planner</h1>
      <h3>Creez, invitez, profitez</h3>
      <p>
        Pas envie de stresser lors de l'organisation de votre mariage ou de
        votre evenement ? Notre application est faite pour vous. Créez votre
        evenement, ajoutez vos invités, personnalisé vos invitations, proposer
        votre cadeau ou laissez vos invités faire des propositions et voter pour
        des cadeaux.
      </p>
      <p>
        Pour commencer connectez vous ou creez votre compte en quelques
        secondes.
      </p>
      <button
        disabled={authenticating}
        onClick={() => signInWithSocialMedia(Providers.google)}
      >
        S'inscrire avec Google
      </button>
      <button
        disabled={authenticating}
        onClick={() => signInWithSocialMedia(Providers.facebook)}
      >
        S'inscrire avec Facebook
      </button>
      <Link to={`/dashboard`}>
        <button>Site</button>
      </Link>
    </div>
  );
};

export default HomePage;
