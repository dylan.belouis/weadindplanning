export default interface IRoute {
  path: string;
  exact: boolean;
  component: React.ComponentType<any>;
  name: string;
  protected: boolean;
}
